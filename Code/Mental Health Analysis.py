#!/usr/bin/env python
# coding: utf-8

# # Mental Health Analysis in Tech Survey
# #By- Aarush Kumar
# #Dated: Sept. 22,2021

# In[1]:


from IPython.display import Image
Image(url='https://c4.wallpaperflare.com/wallpaper/398/120/920/zune-gathering-wallpaper-preview.jpg')


# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import warnings as ws
ws.filterwarnings("ignore")


# ## Importing the required dataset.

# In[3]:


data=pd.read_csv('/home/aarush100616/Downloads/Projects/Mental Health in Tech Survey Analysis/Data/survey.csv')


# In[4]:


data


# In[5]:


data.shape


# In[7]:


data.size


# In[8]:


data.info()


# In[9]:


data.isnull().sum()


# In[10]:


data.describe()


# In[11]:


data.describe().T


# ## Dropping the columns.

# In[13]:


data.drop(columns=['state','comments'],inplace=True)


# ## Imputing the null values.

# In[14]:


print(data['self_employed'].value_counts())
data['self_employed'].fillna('No',inplace=True)


# In[15]:


print(data['work_interfere'].value_counts())
data['work_interfere'].fillna('Sometimes',inplace=True)


# In[16]:


data.columns


# ## Exploratory Data Analysis.

# In[17]:


sns.boxplot(data['Age'])


# In[18]:


data.drop(data[data['Age'] < 0].index, inplace = True) 
data.drop(data[data['Age'] > 100].index, inplace = True) 


# In[19]:


sns.boxplot(data['Age'])


# In[20]:


data['Gender']=[m.lower() for m in data['Gender']]
data['Gender'].value_counts()


# In[21]:


plt.pie(data['Country'].value_counts(),labels=data['Country'].unique(),labeldistance=1.1)


# ## Bivariate and Multivariate Analysis.

# In[22]:


country=data.groupby(data['Country'])


# In[23]:


country['Age'].aggregate(np.median).sort_values()


# In[24]:


country['treatment','remote_work','self_employed'].describe()


# In[25]:


s_employ=data.groupby(['self_employed'])
s_employ['treatment'].describe()


# In[26]:


treat=data.groupby(['treatment'])
treat['Age'].describe()


# In[27]:


data.columns


# In[28]:


treat['tech_company','work_interfere'].describe()


# In[29]:


sns.barplot(data['leave'].unique(),data['leave'].value_counts())


# In[30]:


sns.barplot(data['mental_health_consequence'].unique(),data['mental_health_consequence'].value_counts())


# In[31]:


sns.barplot(data['phys_health_consequence'].unique(),data['phys_health_consequence'].value_counts())


# In[32]:


plt.pie(data['coworkers'].value_counts(),labels=data['coworkers'].unique())
data['coworkers'].value_counts()


# In[37]:


print(data['mental_vs_physical'].value_counts())
plt.hist(data['mental_vs_physical'],histtype='step')


# ## Conclusion and Inferences.

# 1. This case study is majorly based on cases in the Western world.
# 2. Cases show that more than 50% of people surveyed in countries like US,Australia and Canada undergo treatment for mental ailments.
# 3. People who are not more prone to work at home are usually bored and filled with anxiety leading to degradation in mental health.
# 4. People who are in the early 30's usually undergo treatment but there are extreme cases like 8 years and 72 years people recieving the same treatment.
# 5. It is interesting to find that people face mental trauma regardless of whether they are self employed or not.
# 6. The surveyed people agree that their mental health somewhat affects their productivity at work.
# 7. People feel that their employers somewhat easily sanction leave for mental health issues.The reason maybe that the employer does not want to take any risk of overloading the patient with work.
# 8. People feel that sharing about their mental or physical health with employers would help them a bit but they are reluctant to share the same with their coworkers.They would prefer to share with only some of the coworkers.
# 9. People dont know whether the employer considers mental health issues as seriously as the physical ones.The ambiguity still remains about people's reaction towards mental health.
